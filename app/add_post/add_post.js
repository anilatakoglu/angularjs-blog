/**
 * Created by anil on 04.08.2016.
 */

/**
 * Created by anil on 03.08.2016.
 */


angular.module('myApp.add_post', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/category/:id/add_post/', {
            templateUrl: 'add_post/add_post.html',
            controller: 'AddPostCtrl'
        });
    }])

    .controller('AddPostCtrl', function($scope, $http,$routeParams) {
        $scope.post = $routeParams;
        $scope.add_post= function (title,content) {
            $scope.title = angular.copy(title);
            $scope.pst_text=angular.copy(content);

            $scope.author = sessionStorage.getItem("user_id");



            var date =  "2016-07-28T13:56:41.402645Z";

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'

                }
            };
            $http({
                url: 'http://127.0.0.1:8001/api/post/',
                method: "POST",
                data: { 'post_title' : $scope.title,
                        'post_text': $scope.pst_text,
                       // 'updated_date': date,
                        'num_of_like':0,
                        'num_of_dislike':0,
                        'author':  $scope.author,
                        'post_of_category': $scope.post.id}

            },config)
                .then(function(response) {
                        $scope.data = response;
                        window.location='#!/category/' + $scope.post.id + '/';
                    },
                    function(response) { // optional
                        $scope.data = response;
                    });

        }
    });

