/**
 * Created by anil on 03.08.2016.
 */


angular.module('myApp.add_category', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/add_category/', {
            templateUrl: 'add_category/add_category.html',
            controller: 'AddCategoryCtrl'
        });
    }])

    .controller('AddCategoryCtrl', function($scope, $http) {
        $scope.add_cat = function (title) {
            $scope.text_title = angular.copy(title);

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'

                }
            };
            $http({
                url: 'http://127.0.0.1:8001/api/category/',
                method: "POST",
                data: { 'title' : $scope.text_title }
            })
                .then(function(response) {
                        $scope.data = response;
                        window.location='#!/index';
                    },
                    function(response) { // optional
                        $scope.data = response;
                    });

        }
    });

