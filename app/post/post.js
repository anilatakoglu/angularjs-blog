'use strict';

angular.module('myApp.post', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/category/:id/post/:pk', {
            templateUrl: 'post/post.html',
            controller: 'PostCtrl'
        });
    }])

    .controller('PostCtrl', ["$scope", "$routeParams", "$http", function ($scope, $routeParams, $http) {
        $scope.category = $routeParams;
        $scope.post = $routeParams;

        $http.get("http://127.0.0.1:8001/api/post/" + $scope.post.pk)
            .then(function (response) {
                $scope.Posts = response.data;
                $http.get("http://127.0.0.1:8001/api/user/" + $scope.Posts.author)
                    .then(function (response) {
                        $scope.user = response.data;
                    });
            });


        $http.get("http://127.0.0.1:8001/api/comment/")
            .then(function (response) {
                $scope.Comments = response.data;

            });

        $http.get("http://127.0.0.1:8001/api/user/")
            .then(function (response) {
                $scope.Users = response.data;
            });

        $scope.likePost = function () {
            // if($scope.tokens){
            $http.get("http://127.0.0.1:8001/api/post/" + $scope.post.pk + "/like/")
                .then(function (response) {
                    window.location = '#!/category/' + $scope.category.id + '/post/' + $scope.category.pk + '/';

                });
            // }
        };

        $scope.dislikePost = function () {
            // if($scope.tokens){
            $http.get("http://127.0.0.1:8001/api/post/" + $scope.post.pk + "/dislike/")
                .then(function (response) {
                    //$scope.PostDataResponse = data;
                    window.location = '#!/category/' + $scope.category.id + '/post/' + $scope.category.pk + '/';

                });
            // }
        };

        $scope.deletePost = function () {
            // if($scope.tokens){
            $http.delete("http://127.0.0.1:8001/api/post/" + $scope.post.pk + "/")
                .then(function (response) {
                    window.location = '#!/category/' + $scope.category.id + '/';

                });
            // }
        };

        $scope.likeComment = function (cmt_id) {
            $scope.comment_id = angular.copy(cmt_id);
            // if($scope.tokens){
            $http.get("http://127.0.0.1:8001/api/comment/" + $scope.comment_id+ "/like/")
                .then(function (response) {
                    window.location = '#!/category/' + $scope.category.id + '/post/' + $scope.category.pk + '/';

                });
            // }
        };

        $scope.dislikeComment = function (cmt_id) {
            $scope.comment_id = angular.copy(cmt_id);
            // if($scope.tokens){
            $http.get("http://127.0.0.1:8001/api/comment/" + $scope.comment_id+ "/dislike/")
                .then(function (response) {
                    window.location = '#!/category/' + $scope.category.id + '/post/' + $scope.category.pk + '/';
                });
            // }
        };

        $scope.deleteComment = function (cmt_id) {
            $scope.comment_id = angular.copy(cmt_id);
            // if($scope.tokens){
            $http.delete("http://127.0.0.1:8001/api/comment/" + $scope.comment_id + "/")
                .then(function (response) {
                    window.location = '#!/category/' + $scope.category.id + '/post/' + $scope.category.pk + '/';

                });
            // }
        };



    }]);