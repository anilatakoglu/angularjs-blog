'use strict';

// Declare app level module which depends on views, and components

var app = angular.module('myApp', [
  'ngRoute',
  'myApp.category',
  'myApp.post',
  'myApp.home',
  'myApp.version',
  'myApp.login',
  'myApp.sign',
  'myApp.add_category',
  'myApp.add_post',
  'myApp.add_comment',
  'myApp.update_post',
  'myApp.update_comment'

]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');
  $routeProvider.otherwise({redirectTo: '/index'});



}])


  .controller('AppCtrl', ["$scope","$http", function ($scope, $http) {
    $scope.tokens = sessionStorage.getItem("token");
    $scope.username = sessionStorage.getItem("username");
      $scope.logout = function () {
          // if($scope.tokens){
          $http.delete("http://127.0.0.1:8001/api/token/" + $scope.tokens + "/")
              .then(function (response) {
                  //$scope.PostDataResponse = data;
                  location.reload();
                  window.location = "#!/index/";

              });
          // }
      };
  }]);