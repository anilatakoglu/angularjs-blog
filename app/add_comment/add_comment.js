/**
 * Created by anil on 04.08.2016.
 */
angular.module('myApp.add_comment', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/category/:id/post/:pk/add_comment/', {
            templateUrl: 'add_comment/add_comment.html',
            controller: 'AddCommentCtrl'
        });
    }])

    .controller('AddCommentCtrl', function($scope, $http,$routeParams) {
        $scope.post = $routeParams;
        $scope.add_post= function (content) {
            $scope.comment_txt = angular.copy(content);
            //$scope.pst_text=angular.copy(content);

            $scope.author = sessionStorage.getItem("user_id");



            var date =  "2016-07-28T13:56:41.402645Z";

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'

                }
            };
            $http({
                url: 'http://127.0.0.1:8001/api/comment/',
                method: "POST",
                data: { 'comment_text' : $scope.comment_txt,
                    'number_of_like':0,
                    'number_of_dislike':0,
                    'author':  $scope.author,
                    'post_of_comment': $scope.post.pk}

            },config)
                .then(function(response) {
                        $scope.data = response;
                        window.location='#!/category/' + $scope.post.id + '/post/' + $scope.post.pk + '/';
                    },
                    function(response) { // optional
                        $scope.data = response;
                    });

        }
    });
