/**
 * Created by anil on 05.08.2016.
 */

angular.module('myApp.update_post', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/category/:id/post/:pk/update', {
            templateUrl: 'update_post/update_post.html',
            controller: 'UpdatePostCtrl'
        });
    }])

    .controller('UpdatePostCtrl', function($scope, $http,$routeParams) {
        $scope.post = $routeParams;
        $scope.update_post= function (title,content) {
            $scope.title = angular.copy(title);
            $scope.pst_text=angular.copy(content);

            //$scope.author = sessionStorage.getItem("user_id");



            var date =  "2016-07-28T13:56:41.402645Z";

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'

                }
            };
            $http({
                url: "http://127.0.0.1:8001/api/post/" +$scope.post.pk+ "/",
                method: "PUT",
                data: { 'post_title' : $scope.title,
                    'post_text': $scope.pst_text,
                    // 'updated_date': date,
                    'num_of_like':$scope.post.num_of_like,
                    'num_of_dislike':$scope.post.num_of_dislike,
                    'author':  $scope.post.author,
                    'post_of_category': $scope.post.id}

            },config)
                .then(function(response) {
                        $scope.data = response;
                        window.location='#!/category/' + $scope.post.id + '/post/' + $scope.post.pk + '/';
                        //$scope.PostDataResponse = data;
                    },
                    function(response) { // optional
                        $scope.data = response;
                        //$scope.PostDataResponse = data;
                    });

        }
    });

