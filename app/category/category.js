'use strict';

angular.module('myApp.category', ['ngRoute'])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/category/:id', {
    templateUrl: 'category/category.html',
    controller: 'CategoryCtrl'
  });
}])

.controller('CategoryCtrl', ["$scope", "$routeParams", "$http", function($scope,$routeParams, $http) {
    $scope.category = $routeParams;

    $http.get("http://127.0.0.1:8001/api/post/")
      .then(function(response) {
        $scope.Posts = response.data;
      });
    $http.get("http://127.0.0.1:8001/api/category/")
        .then(function(response) {
            $scope.Categories = response.data;

        });




}]);
