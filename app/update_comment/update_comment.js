/**
 * Created by anil on 05.08.2016.
 */

angular.module('myApp.update_comment', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/category/:id/post/:pk/comment/:c_id/update', {
            templateUrl: 'update_comment/update_comment.html',
            controller: 'UpdateCommentCtrl'
        });
    }])

    .controller('UpdateCommentCtrl', function($scope, $http,$routeParams) {
        $scope.comment = $routeParams;
        $http.get("http://127.0.0.1:8001/api/comment/" + $scope.comment.c_id + "/")
            .then(function (data) {
                $scope.Comment_z = data.data;
            });
        $scope.update_comment= function (content) {
            $scope.comment_txt = angular.copy(content);

            //$scope.author = sessionStorage.getItem("user_id");

            var date =  "2016-07-28T13:56:41.402645Z";

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'

                }
            };
            $http({
                url: "http://127.0.0.1:8001/api/comment/"+$scope.comment.c_id+"/",
                method: "PUT",
                data: { 'comment_text' : $scope.comment_txt,
                    'number_of_like':$scope.Comment_z.number_of_like,
                    'number_of_dislike':$scope.Comment_z.number_of_dislike,
                    'author':  $scope.Comment_z.author,
                    'post_of_comment': $scope.Comment_z.post_of_comment}

            },config)
                .then(function(response) {
                    $scope.data = response;
                    window.location='#!/category/' + $scope.comment.id + '/post/' + $scope.comment.pk + '/';
                    },
                    function(response) { // optional
                        $scope.data = response;
                    });

        }
    });