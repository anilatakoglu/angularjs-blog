
/**
 * Created by anil on 02.08.2016.
 */

angular.module('myApp.login', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/login/', {
            templateUrl: 'login/login.html',
            controller: 'LoginCtrl'
        });
    }])



.controller('LoginCtrl', ["$scope", "$routeParams", "$http", function ($scope, $routeParams, $http) {

    $scope.login = function (username, password) {
        $scope.text_username = angular.copy(username);
        $scope.text_password = angular.copy(password);


        // var config = {
        //     headers : {
        //         'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;',
        //         'WWW-Authorization': 'Token'+ $scope.token
        //     }
        // };

        $http.post("http://127.0.0.1:8001/api/auth/", {username : $scope.text_username, password: $scope.text_password})
            .success(function (data, status, headers, config) {
                $scope.PostDataResponse = data;
                sessionStorage.setItem("token", $scope.PostDataResponse.token);
                sessionStorage.setItem("user_id", $scope.PostDataResponse.user_id);
                sessionStorage.setItem("username", $scope.PostDataResponse.username);
                location.reload();
                window.location = "#!/index/";
            })
            .error(function (data, status, header, config) {
                $scope.ResponseDetails = "Data: " + data +
                    "<hr />status: " + status +
                    "<hr />headers: " + header +
                    "<hr />config: " + config;
            });
    }
}]);

