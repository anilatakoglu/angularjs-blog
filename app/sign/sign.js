/**
 * Created by anil on 02.08.2016.
 */


angular.module('myApp.sign', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/register/', {
            templateUrl: 'sign/sign.html',
            controller: 'RegisterCtrl'
        });
    }])

    .controller('RegisterCtrl', function($scope, $http) {
        // create a blank object to handle form data.
        //$scope.user = {};
        // calling our submit function.
        $scope.submitForm = function(a,b,c){
            $scope.username = angular.copy(a);
            $scope.email=angular.copy(b);
            $scope.password=angular.copy(c);
            // Posting data to php file
            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'

                }
            };
            $http({
                url: 'http://127.0.0.1:8001/api/user/',
                method: "POST",
                data: { 'username' : $scope.username,
                        'password': $scope.password,
                        'email': $scope.email}

            },config)
                .then(function(response) {
                        $scope.data = response;
                        window.location='#!/index/';
                    },
                    function(response) { // optional
                        $scope.data = response;
                    });
        };
    });

