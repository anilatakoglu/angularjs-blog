/**
 * Created by anil on 01.08.2016.
 */
'use strict';

angular.module('myApp.home', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/index/', {
            templateUrl: 'home/home.html',
            controller: 'HomeCtrl'
        });
    }])

    .controller('HomeCtrl', ["$scope", "$routeParams", "$http", function($scope,$routeParams, $http) {
        //$scope.category = $routeParams;

        $http.get("http://127.0.0.1:8001/api/category/")
            .then(function(response) {
                $scope.Categories = response.data;

            });

        $http.get("http://127.0.0.1:8001/api/post/")
            .then(function(response) {
                $scope.Posts = response.data;
            });

        $scope.deleteCategory = function (id) {
            $scope.id = angular.copy(id);

            $http.delete("http://127.0.0.1:8001/api/category/" + $scope.id + "/")
                .then(function (response) {
                    window.location = '#!/home/';

                });
        }

    }]);